# bitround parameters

This repository collects knowledge about bitrounding parameters.
The current focus is on good `keepbits` values for ICON model output.

Please create a merge request to add your own values if you have more.

## keepbits
We collect keepbits in groups of desired quality and they can be found in `keepbits/<quality>.yaml`.
Currently we have:

* `default`: should be a good default, relative and absolute errors should be below 1%, zonal and global means are below 1%
